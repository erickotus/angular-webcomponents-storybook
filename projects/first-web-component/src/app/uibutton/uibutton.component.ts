import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-uibutton',
  templateUrl: './uibutton.component.html',
  styleUrls: ['./uibutton.component.scss']
})
export class UIButtonComponent implements OnInit {

  @Input() label?: string;
  @Input() color?: string;
  @Output()
  onClick = new EventEmitter<Event>();

  constructor() { }

  ngOnInit(): void {
  }

}
