import { Story, Meta } from '@storybook/angular/types-6-0';
import { UIButtonComponent } from '../../projects/first-web-component/src/app/uibutton/uibutton.component';
import {moduleMetadata} from "@storybook/angular";
import {MatButtonModule} from "@angular/material/button";
import {action} from "@storybook/addon-actions";

export default {
  title: 'UIButton',
  component: UIButtonComponent,
  argTypes: {
    backgroundColor: { control: 'color' },
    onClick: {action: 'clicked'}
  },
  decorators: [moduleMetadata({
    imports: [MatButtonModule]
  })]
} as Meta;

const Template: Story<UIButtonComponent> = (args: UIButtonComponent) => ({
  props: args,
});

export const Primary = Template.bind({});
Primary.args = {
  color: "primary",
  label: 'Primary'
};

export const Secondary = Template.bind({});
Secondary.args = {
  color: "secondary",
  label: 'Secondary',
};
